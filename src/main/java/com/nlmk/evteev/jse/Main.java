package com.nlmk.evteev.jse;

import com.nlmk.evteev.jse.service.NumericService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    private static Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        NumericService service = new NumericService();
        System.out.println("Команды:");
        System.out.println("1 - Умножение;");
        System.out.println("2 - Факториал;");
        System.out.println("3 - Числа Фибоначчи;");
        System.out.println("4 - Завершение работы.");
        boolean flag = true;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            while (flag) {
                System.out.println("Введите номер команды");
                String value = bufferedReader.readLine();
                StringBuilder arg1 = new StringBuilder();
                StringBuilder arg2 = new StringBuilder();
                switch (value) {
                    case ("1"):
                        System.out.println("Умножение");
                        System.out.println("Первое число");
                        arg1.append(bufferedReader.readLine());
                        System.out.println("Второе число");
                        arg2.append(bufferedReader.readLine());
                        System.out.println(arg1 + "+" + arg2 + "=" + service.multiplication(arg1.toString(), arg2.toString()));
                        break;
                    case ("2"):
                        System.out.println("Факториал");
                        System.out.println("Введите число");
                        arg1.append(bufferedReader.readLine());
                        System.out.println("Введите кол-во потоков для расчета");
                        arg2.append(bufferedReader.readLine());
                        try {
                            System.out.println(arg1 + "!=" + service.factorial(arg1.toString(), Integer.parseInt(arg2.toString())));
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }
                        break;
                    case ("3"):
                        System.out.println("Фибоначчи");
                        System.out.println("Введите число");
                        arg1.append(bufferedReader.readLine());
                        System.out.println("Fib " + arg1 + "=" + Arrays.toString(service.fibonacci(arg1.toString())));
                        break;
                    case ("4"):
                        System.out.println("Выход");
                        flag = false;
                        break;
                    default:
                        logger.info("Команда не определена");
                }
                arg1.setLength(0);
                arg2.setLength(0);
            }
        } catch (IOException | NumberFormatException ex) {
            logger.log(Level.SEVERE, ex.getMessage());
        }
    }
}
