package com.nlmk.evteev.jse.service;

import java.math.BigInteger;
import java.util.concurrent.Callable;

public class FactorialService implements Callable<BigInteger> {

    private final Integer start;
    private final Integer end;

    public FactorialService(Integer start, Integer end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public BigInteger call() {
        BigInteger result = BigInteger.ONE;
        for (int j = start; j <= end; j++) {
            result = result.multiply(BigInteger.valueOf(j));
        }
        return result;
    }
}
