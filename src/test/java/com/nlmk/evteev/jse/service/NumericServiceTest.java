package com.nlmk.evteev.jse.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.*;

class NumericServiceTest {

    public static final String VAR_1 = "2";
    public static final String VAR_2 = "4";
    public static final String STRING = "a";
    public static final String VAR_11 = "152428";
    public static final String VAR_21 = "395498";
    public static final String NEGATIVE_STRING = "-5";
    public static final String EMPTY_STRING = "";

    private NumericService numericService;

    @BeforeEach
    private void init() {
        numericService = new NumericService();
    }

    @Test
    void multiplicationGood() {
        assertEquals(8, numericService.multiplication(VAR_1, VAR_2));
    }

    @Test
    void multiplicationEmptyParam() {
        assertThrows(IllegalArgumentException.class, () -> numericService.multiplication(EMPTY_STRING, VAR_2));
    }

    @Test
    void multiplicationParseException() {
        assertThrows(IllegalArgumentException.class, () -> numericService.multiplication(STRING, VAR_1));
    }

    @Test
    void multiplicationArithmeticException() {
        assertThrows(IllegalArgumentException.class, () -> numericService.multiplication(VAR_11, VAR_21));
    }

    @Test
    void factorialGood() throws ExecutionException, InterruptedException {
        assertEquals(BigInteger.valueOf(120), numericService.factorial("5", 3));
    }

    @Test
    void factorialParamIsNullOrEmpty() {
        assertThrows(IllegalArgumentException.class, () -> numericService.factorial(null, 3));
        assertThrows(IllegalArgumentException.class, () -> numericService.factorial(EMPTY_STRING, 3));
    }

    @Test
    void factorialParseException() {
        assertThrows(IllegalArgumentException.class, () -> numericService.factorial(STRING, 3));
    }

    @Test
    void fibonacciGood() {
        long[] expected = {0, 1, 1, 2, 3, 5};
        long[] result = numericService.fibonacci("5");
        assertArrayEquals(expected, result);
    }

    @Test
    void fibonacciParamIsNullOrEmpty() {
        assertThrows(IllegalArgumentException.class, () -> numericService.fibonacci(EMPTY_STRING));
        assertThrows(IllegalArgumentException.class, () -> numericService.fibonacci(null));
    }

    @Test
    void fibonacciParamIsNegative() {
        assertThrows(IllegalArgumentException.class, () -> numericService.fibonacci(NEGATIVE_STRING));
    }

    @Test
    void fibonacciParseException() {
        assertThrows(IllegalArgumentException.class, () -> numericService.fibonacci(STRING));
    }

}